package ua.kpi.fpm.pzks.FS;

import java.util.concurrent.Semaphore;

public class NamedPipe extends Entity {
    private String data;
    private Semaphore semaphoreReader;
    private Semaphore semaphoreWriter;


    private NamedPipe(String name, Directory parent) {
        super(name, parent);
        data = "";
        semaphoreReader = new Semaphore(1);
        semaphoreWriter = new Semaphore(1);
        semaphoreReader.acquireUninterruptibly();
    }

    public static NamedPipe create(String name, Directory parent) {
        return new NamedPipe(name, parent);
    }

    public void write(String data) {
        if (data == null) {
            throw new IllegalArgumentException("Data should not be null");
        }
        semaphoreWriter.acquireUninterruptibly();
        this.data = data;
        semaphoreReader.release();
    }

    public String read() {
        semaphoreReader.acquireUninterruptibly();
        var res = data;
        semaphoreWriter.release();
        return res;
    }


    @Override
    public EntityType getType() {
        return EntityType.NAMED_PIPE;
    }
}
