package ua.kpi.fpm.pzks;

import ua.kpi.fpm.pzks.FS.*;
import ua.kpi.fpm.pzks.threads.ThreadTask;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.function.Supplier;

public class Main {
    public static void main(String[] args) {
        System.out.println("Lab 1");
        System.out.println("Task 2");
        var root = Directory.create("root", null);
        var userDir = Directory.create("user", Directory.create("home", root));
        var sysDir = Directory.create("sys", root);
        var binaryFile = BinaryFile.create("data.bin", sysDir, "dat".getBytes());
        var bufferFile = BufferFile.<Character>create("buff.sys", sysDir);
        var logFile = LogTextFile.create("sample.log", root, "some log\n");
        var namedPipe = NamedPipe.create("pipe", sysDir);

        fsTraverse(root, 0);

        logFile.append("Hello\n");
        logFile.append("some data\n");
        logFile.move(userDir);

        System.out.println();
        fsTraverse(root, 0);

        System.out.println();
        userDir.list().forEach((x) ->  {
            if (x.getType() == Entity.EntityType.LOG_TEXT_FILE) {
                System.out.println(((LogTextFile)x).read());
            }
        });

        binaryFile.move("/");

        System.out.println();
        fsTraverse(root, 0);

        System.out.println(new String(binaryFile.read()));

        bufferFile.push('V');
        bufferFile.push('A');
        System.out.println(bufferFile.consume());

        userDir.move("/sys");
        System.out.println();
        fsTraverse(root, 0);

        namedPipe.write("data pipe");
        System.out.println(namedPipe.read());
        System.out.println("=====================================");
        System.out.println("Task 3");
        ThreadTask one = new ThreadTask(1) {
            @Override
            protected void phaseTwo() {
                try {
                    countDownLatchCount2.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        ThreadTask two = new ThreadTask(2) {
            @Override
            protected void phaseTwo() {
                try {
                    countDownLatchCount2.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        ThreadTask three = new ThreadTask(3) {
            @Override
            protected void phaseTwo() { }

            @Override
            public void phaseThree() {
                countDownLatchCount2.countDown();
                super.phaseThree();
            }
        };
        ThreadTask four = new ThreadTask(4) {
            @Override
            protected void phaseTwo() { }

            @Override
            public void phaseThree() {
                countDownLatchCount2.countDown();
                super.phaseThree();
            }
        };
        ThreadTask five = new ThreadTask(5) {
            @Override
            protected void phaseTwo() {
                try {
                    countDownLatchCount4.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void phaseThree() { }
        };
        one.start();
        two.start();
        three.start();
        four.start();
        five.start();

        try {
            one.join();
            two.join();
            three.join();
            four.join();
            five.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private static void fsTraverse(Directory dir, int indent) {
        System.out.println(" ".repeat(indent) + entityPrint(dir));
        indent += 4;
        for (var cur : dir.list()) {
            if (cur.isDirectory()) {
                fsTraverse((Directory) cur, indent);
            } else {
                System.out.println(" ".repeat(indent) + entityPrint(cur));
            }
        }
    }

    private static String entityPrint(Entity e) {
        return "-> " + e.getType().toString() + ": " + e.getName();
    }
}
